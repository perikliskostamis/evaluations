import AccessToken = require('./access-token-model');
/**
 * @class
 */
declare class EvaluationAccessToken extends AccessToken {

     
     public evaluationEvent: any;
     
     /**
      * @description An  access token is a string representing an authorization issued to the client.
      */
     public access_token: string; 

}

export = EvaluationAccessToken;
