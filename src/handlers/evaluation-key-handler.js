import { HttpTokenExpiredError } from '@themost/common';
const EvaluationAccessToken = require('../models/evaluation-access-token-model');

 export class EvaluationKeyHandler {
    static createInstance() {
        return new EvaluationKeyHandler();
    }

    beginRequest(req, res, callback) {
        try {
            if (Object.prototype.hasOwnProperty.call(req.headers, 'evaluation-key')) {
                const evaluationKey = req.headers['evaluation-key'].replace(/-/ig, '');
                return EvaluationAccessToken.inspect(req.context, evaluationKey).then((token) => {
                    if (token.active === false) {
                        return callback(new HttpTokenExpiredError('Evaluation access token has been expired'))
                    }
                    req.context.user = req.context.user || {
                        name: 'anonymous',
                        authenticationType: 'Evaluation',
                        authenticationToken: token.access_token,
                        authenticationScope: token.scope
                    };
                    return callback();
                }).catch((err) => {
                    return callback(err);
                });
            }
            return callback();
        } catch(error){
            return callback(error);
        }
    }

     beginRequestAsync(req, res) {
         return new Promise(((resolve, reject) => {
            this.beginRequest( req, res, function(err) {
                if (err) {
                    return reject(err);
                }
                return resolve(err);
             });
         }));

     }
}
