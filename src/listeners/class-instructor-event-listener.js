import { DataObjectState } from '@themost/data';
/**
 * 
 * @param {DataEventArgs} event 
 */
async function beforeSaveAsync(event) {
    // get organizer from courseClass
    const context = event.model.context;
    if (event.target.courseClassInstructor) {
        const organizer = await context.model('CourseClassInstructor')
            .where('id').equal(event.target.courseClassInstructor)
            .select('courseClass/course/department').value();
        event.target.organizer = organizer;
    }
    if (event.state===DataObjectState.Update)
    {
        const previous = event.previous;
        // check if endDate is changed and update expired field for tokens
        if (event.target.endDate) {
            const prevEndDate = event.previous.endDate;
            const endDate = event.target.endDate;
            prevEndDate.setHours(0, 0, 0, 0);
            endDate.setHours(0, 0, 0, 0);
            if (prevEndDate-endDate!==0) {
                let tokens = await context.model('EvaluationAccessToken').where('evaluationEvent').equal(event.target.id)
                    .and('used').notEqual(1)
                    .silent().getItems();
                if (tokens.length > 0) {
                    tokens = tokens.map(x => {
                        x.expires = event.target.endDate;
                        return x;
                    });
                    // update tokens
                    await context.model('EvaluationAccessToken').silent().save(tokens);
                }
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function beforeSave(event, callback) {
    beforeSaveAsync(event).then(() => {
        return callback()
    }).catch( err => {
        return callback(err);
    })
}

