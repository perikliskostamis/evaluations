import {DataObject, EdmMapping, EdmType} from '@themost/data';
import _ from "lodash";
import path from "path";
import {TraceUtils} from "@themost/common/utils";
import {DataNotFoundError, HttpServerError} from "@themost/common/errors";
/**
 * @class
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('EvaluationDocument')
class EvaluationDocument extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    static async next(context) {
        return await new Promise((resolve, reject) => {
            context.db.nextIdentity('EvaluationDocument', 'documentNumber', (err, value) => {
                if (err) {
                    return reject(err);
                }
                return resolve(value);
            });
        });
    }

    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('extraAttributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('Add', 'EvaluationDocument')
    static async add(context, file, extraAttributes) {
        const attachment = Object.assign({
            name: file.contentFileName
        }, file, extraAttributes);
        return await EvaluationDocument._addAttachment(context, attachment);
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    @EdmMapping.param('attachment', 'EvaluationDocument', true, true)
    @EdmMapping.action('RemoveAttachment', 'EvaluationDocument')
    static async removeAttachment(context, attachment) {
        return await EvaluationDocument._removeAttachment(context,attachment.id);
    }

   static _addAttachment(context, file) {
        let finalResult;
        return new Promise((resolve, reject)=> {
            context.db.executeInTransaction((callback)=> {
                if (typeof file.originalname === 'string') {
                    file.name = file.originalname.replace(/\.[A-Z0-9]+$/ig, function (x) {
                        return x.toLowerCase();
                    });
                }
                const mime = file.mimetype || file.contentType;
                if (_.isNil(mime)) {
                    return reject(new Error('The specified file type is not supported by the system.'))
                }
                const addAttributes = {};
                _.forEach(context.model(file.additionalType || 'EvaluationDocument').attributeNames, (attribute) => {
                    if (Object.prototype.hasOwnProperty.call(file, attribute)) {
                        addAttributes[attribute] = file[attribute];
                    }
                });
                const newAttachment = _.assign(addAttributes, {contentType: mime});
                const svc = context.getApplication().getService(function PrivateContentService(){});
                //add attachment to attachments
                let filePath;
                if (file.destination && file.filename) {
                    filePath = path.resolve(file.destination, file.filename);
                } else {
                    filePath = file.path;
                }
                newAttachment.additionalType = file.additionalType || 'EvaluationDocument';
                svc.copyFrom(context, filePath, newAttachment, function (err) {
                    if (err) {
                        TraceUtils.error(err);
                        return callback(new HttpServerError());
                    }
                    svc.resolveUrl(context, newAttachment, function (err, url) {
                        if (err) {
                            return callback(err);
                        } else {
                            finalResult = _.assign({}, newAttachment, {
                                url: url,
                                name: newAttachment.filename
                            });
                            return callback();
                        }
                    });

                });
            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        })

    }

    removeAttachment(context, id) {
        let result;
        return new Promise((resolve, reject)=> {
            context.db.executeInTransaction((callback)=> {
                //get attachment
                return context.model('Attachment').where('id').equal(id).getItem().then((attachment)=> {
                    if (attachment) {
                        //remove attachment connection
                        return self.property('attachments').remove(attachment,(err)=> {
                            //remove attachment
                            if (err) {
                                return callback(err);
                            }
                            return context.model('Attachment').silent().remove(attachment).then(()=> {
                                result = attachment;
                                return callback();
                            }).catch((err)=> {
                                return callback(err);
                            });
                        });
                    }
                    return callback(new DataNotFoundError());
                }).catch((err)=> {
                    return callback(err);
                });
            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(result);
            });
        });
    }

}
module.exports = EvaluationDocument;
